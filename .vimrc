"VIM Configuration File
" Description: Optimized for Golang & PHP development, but useful also for other things.
" Author: Don B. Stringham
"
try
    source /Users/donbstringham/.vim/config/00-init.vimrc
    source /Users/donbstringham/.vim/config/01-general.vimrc
    source /Users/donbstringham/.vim/config/02-plugins.vimrc
    source /Users/donbstringham/.vim/config/03-keys.vimrc
    source /Users/donbstringham/.vim/config/04-line.vimrc
    source /Users/donbstringham/.vim/config/05-misc.vimrc
catch
endtry
